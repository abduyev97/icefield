package entity.items;

public class FlareGun implements Item {
    private int completedParts;
    private boolean isFlareGunComlete;

    public void addPart() {
        completedParts++;
        if(completedParts==3){
            this.isFlareGunComlete = true;
        }
    }
}
