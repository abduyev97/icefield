package entity.items;

import entity.Iceberg;

public class Shovel implements Item {
    public void removeSnow(Iceberg iceberg) {
        iceberg.removeSnow(2);
    }
}
