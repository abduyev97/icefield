package entity.items;

import entity.Figure;

public class Food implements Item {
    public int increaseBodyHeat(Figure figure) {
        figure.increaseBodyHeat();
        return figure.getBodyHeat();
    }
}
