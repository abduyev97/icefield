package entity;

public class Player {
    private int remainedMoves;
    private String username;
    private Figure playingFigure;

    public Player(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPlayingFigure(Figure figure) {
        playingFigure = figure;
    }

    public Figure getPlayingFigure() {
        return playingFigure;
    }
}
