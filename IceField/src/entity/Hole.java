package entity;

import java.util.ArrayList;

public class Hole {
    private ArrayList<Figure> fallenFigures;
    private int capacity;

    public Hole() {
        this.capacity = 0;
        this.fallenFigures = new ArrayList<Figure>();
    }

    public void fallFigures(Figure figure) {
        while(figure.getBodyHeat() > 0) {
            figure.decreaseBodyHeat();
        }
        fallenFigures.add(figure);
    }

    public ArrayList<Figure> getFallenFigures() {
        return fallenFigures;
    }
}
