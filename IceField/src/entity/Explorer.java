package entity;

public class Explorer extends Figure {
    public Explorer() {
        super(4);
    }
    public int checkCapacity(Iceberg ic) {
        return ic.getCapacity();
    }
}
