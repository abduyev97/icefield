package entity;

import entity.items.Item;
import entity.moves.MoveTo;

import java.util.ArrayList;

public class Figure {
    protected int bodyHeat;
    private boolean isFallen;
    private boolean isTaken;
    private Iceberg onIceberg;
    private MoveTo moveTo;
    private ArrayList<Item> items;
    private int ID;
    protected boolean insideIgloo;


    public Figure(int bodyHeat) {
        this.moveTo = new MoveTo();
        this.bodyHeat = bodyHeat;
        this.items = new ArrayList<Item>();
        this.insideIgloo=false;
    }

    public void setTaken() {
        isTaken = true;
    }

    public boolean getTaken() {
        return isTaken;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void fall() {
        while (isFallen) {
            bodyHeat--;
        }
    }

    public boolean isFallen() {
        return isFallen;
    }

    public void setFallen(boolean fallen) {
        isFallen = fallen;
    }

    public int getBodyHeat() {
        return bodyHeat;
    }

    public void collectItem(Item item) {
        items.add(item);
    }

    public boolean hasItem() {
        return true;
    }

    public void increaseBodyHeat() {
        ++this.bodyHeat;
    }

    public void decreaseBodyHeat() {
        --this.bodyHeat;
    }

    public void setOnIceberg(Iceberg onIceberg) {
        this.onIceberg = onIceberg;
    }

    public Iceberg getOnIceberg() {
        return onIceberg;
    }

    public void setBodyHeat(int bodyHeat) {
        this.bodyHeat = bodyHeat;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public MoveTo getMoveTo() {
        return this.moveTo;
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

    public boolean isInsideIgloo() {
        return insideIgloo;
    }

    public void setInsideIgloo(boolean insideIgloo) {
        this.insideIgloo = insideIgloo;
    }

}
