package entity;

import entity.items.Item;

import java.util.ArrayList;

import static java.lang.Integer.max;

public class Iceberg {
    private int capacity;
    private boolean isStable;
    private boolean iglooBuilt;
    private int amountOfSnow;
    private int number;
    private ArrayList<Iceberg> neighbours;
    private ArrayList<Figure>figures;
    private ArrayList<Eskimos>eskimos;
    private ArrayList<Explorer>explorers;
    private ArrayList<Item>items;


    public Iceberg(boolean isStable, int capacity, int amountOfSnow, int number) {
        if (isStable) this.isStable = true;
        else this.isStable = false;

        this.capacity = capacity;
        this.amountOfSnow = amountOfSnow;
        this.number = number;
        this.neighbours = new ArrayList<Iceberg>();
        this.figures = new ArrayList<Figure>();
        this.eskimos = new ArrayList<Eskimos>();
        this.explorers = new ArrayList<Explorer>();
        this.items = new ArrayList<Item>();
        this.iglooBuilt=false;
    }

    public void removeSnow(int unit) {
        if (this.amountOfSnow >= 1) {
            this.amountOfSnow -= unit;
            this.amountOfSnow = max(amountOfSnow, 0);
        }
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public void addEskimos(Eskimos eskimo) {
        eskimos.add(eskimo);
        figures.add(eskimo);
    }

    public void addExplorers(Explorer explorer) {
        explorers.add(explorer);
        figures.add(explorer);
    }

    public void removeEskimos(Eskimos eskimo) {
        eskimos.remove(eskimo);
        figures.remove(eskimo);
    }

    public void removeExplorer(Explorer explorer) {
        explorers.remove(explorer);
        figures.remove(explorer);
    }

    public void addNeighbour(Iceberg iceberg) {
        this.neighbours.add(iceberg);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isStable() {
        return isStable;
    }

    public void setStable(boolean isStable) {
        this.isStable = isStable;
    }

    public ArrayList<Eskimos> getEskimos () {
        return eskimos;
    }

    public ArrayList<Explorer> getExplorers() {
        return explorers;
    }

    public ArrayList<Figure> getFigures() {
        return figures;
    }

    public int getAmountOfSnow() {
        return amountOfSnow;
    }

    public int getNumber() {
        return this.number;
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

    public boolean isIglooBuilt() {
        return iglooBuilt;
    }

    public void setIglooBuilt(boolean iglooBuilt) {
        this.iglooBuilt = iglooBuilt;
    }
}
