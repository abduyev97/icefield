package entity;

public class Blizzard {
    private boolean isActive;

    /**
     *
     * @param ic
     */
    public void killFigures(Iceberg ic) {
        if(isActive){
            for (Eskimos e : ic.getEskimos()
            ) {
                if(!e.hasIgloo() || !e.isInsideIgloo()){
                    e.setBodyHeat(0);
                    e.getOnIceberg().removeEskimos(e);
                }
            }
            for (Explorer ex : ic.getExplorers()
            ) {
                if(!ex.isInsideIgloo())
                ex.setBodyHeat(0);
                ex.getOnIceberg().removeExplorer(ex);
            }
        }
    }

    public void disable() {
        this.isActive = false;
    }

    public void enable() {
        this.isActive = true;
    }
}
