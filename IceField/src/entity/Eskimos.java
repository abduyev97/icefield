package entity;

public class Eskimos extends Figure {
    private boolean hasIgloo;
    int bodyHeat = 5;

    public Eskimos() {
        super(5);
    }
    public void buildIgloo(Iceberg iceberg) {
        hasIgloo = true;
        iceberg.setIglooBuilt(true);
    }

    public boolean hasIgloo() {
        return hasIgloo;
    }
}
