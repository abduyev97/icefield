package entity.moves;

import entity.Eskimos;
import entity.Explorer;
import entity.Figure;
import entity.Iceberg;

public class MoveTo implements IMove {
    public int stepTo(Iceberg steppedFromIceberg, Iceberg steppedToIceberg, Figure figure, int figure_number) {
        if (steppedToIceberg.getCapacity() >= steppedToIceberg.getFigures().size() + 1) {
            if (figure_number % 2 == 1) {
                figure.setOnIceberg(steppedToIceberg);
                steppedFromIceberg.removeEskimos((Eskimos) figure);
                steppedToIceberg.addEskimos((Eskimos) figure);
            } else {
                figure.setOnIceberg(steppedToIceberg);
                steppedFromIceberg.removeExplorer((Explorer) figure);
                steppedToIceberg.addExplorers((Explorer) figure);
            }
        }
        else {
            System.out.println("Iceberg does not have enough capacity to hold figures");
            return -1;
        }
        figure.setInsideIgloo(false);
        return figure.getOnIceberg().getNumber();
    }
}
