import entity.*;
import entity.items.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private int number_of_players;
    private int number_of_figures;
    private int number_of_icebergs;
    private int number_of_items;
    private ArrayList<Player> players;
    private ArrayList<Figure> figures;
    private ArrayList<Eskimos> eskimos;
    private ArrayList<Explorer> explorers;
    private ArrayList<Iceberg> icebergs;
    private ArrayList<Item> items;

    public static Scanner in = new Scanner(System.in);

    public Game() {
        this.number_of_players = 0;
        this.number_of_figures = 6;
        this.number_of_icebergs = 6;
        this.number_of_items = 8;
        this.players = new ArrayList<Player>();
        this.figures = new ArrayList<Figure>();
        this.eskimos = new ArrayList<Eskimos>();
        this.explorers = new ArrayList<Explorer>();
        this.icebergs = new ArrayList<Iceberg>();
        this.items = new ArrayList<Item>();

    }

//    public void initializeParameters() {
//        System.out.println("Enter the number of players: ");
//        this.number_of_players = in.nextInt();
//        in.nextLine();
//        for (int i = 1; i <= number_of_players; ++i) {
//            System.out.println("Name of player" + i + ": ");
//            String username = in.nextLine();
//            Player player = new Player(username);
//            players.add(player);
//         }
//
//        for (int i=1; i <= number_of_figures; ++i) {
//            if (i%2 == 1) {
//                Eskimos eskimo = new Eskimos();
//                eskimos.add(eskimo);
//                figures.add(eskimo);
//            } else {
//                Explorer explorer = new Explorer();
//                explorers.add(explorer);
//                figures.add(explorer);
//            }
//            figures.get(i-1).setID(i);
//            System.out.println(figures.get(i-1).getID());
//        }
//
//        Iceberg previous = null;
//        for (int i = 1; i <= number_of_icebergs; ++i) {
//            boolean isStable;
//            int capacity;
//            int amountOfSnow;
//            int number = i;
//            if (i%2 == 1) {
//                isStable = true;
//                capacity = Integer.MAX_VALUE;
//                amountOfSnow = generateRandomNumber(5);
//            } else {
//                isStable = false;
//                capacity = generateRandomNumber(this.number_of_figures);
//                amountOfSnow = generateRandomNumber(5);
//            }
//            Iceberg iceberg = new Iceberg(isStable, capacity, amountOfSnow, number);
//            icebergs.add(iceberg);
//            if (previous != null) {
//                iceberg.addNeighbour(previous);
//                previous.addNeighbour(iceberg);
//            }
//            previous = iceberg;
//        }
//        System.out.println("Enter the number of items to be placed in the field:");
//        int numberOfItems=in.nextInt();
//        for (int i=0; i < numberOfItems; ++i) {
//            Rope rope = new Rope();
//            Shovel shovel = new Shovel();
//            DivingSuit divingSuit = new DivingSuit();
//            Food food = new Food();
//            items.add(rope);
//            items.add(shovel);
//            items.add(divingSuit);
//            items.add(food);
//        }
//
//        placeFiguresOnIcebergs();
//        placeItemsOnIcebergs();
    //   }

    public void startGame() {
        System.out.println("Enter the number of players: ");
        this.number_of_players = in.nextInt();
        in.nextLine();
        for (int i = 1; i <= number_of_players; ++i) {
            System.out.println("Name of player" + i + ": ");
            String username = in.nextLine();
            Player player = new Player(username);
            players.add(player);
        }

        for (int i = 1; i <= number_of_figures; ++i) {
            if (i % 2 == 1) {
                Eskimos eskimo = new Eskimos();
                eskimos.add(eskimo);
                figures.add(eskimo);
            } else {
                Explorer explorer = new Explorer();
                explorers.add(explorer);
                figures.add(explorer);
            }
            figures.get(i - 1).setID(i);
            //    System.out.println(figures.get(i-1).getID());
        }

        Iceberg previous = null;
        for (int i = 1; i <= number_of_icebergs; ++i) {
            boolean isStable;
            int capacity;
            int amountOfSnow;
            int number = i;
            if (i % 2 == 1) {
                isStable = true;
                capacity = Integer.MAX_VALUE;
                amountOfSnow = generateRandomNumber(5);
            } else {
                isStable = false;
                capacity = generateRandomNumber(this.number_of_figures);
                amountOfSnow = generateRandomNumber(5);
            }
            Iceberg iceberg = new Iceberg(isStable, capacity, amountOfSnow, number);
            icebergs.add(iceberg);
            if (previous != null) {
                iceberg.addNeighbour(previous);
                previous.addNeighbour(iceberg);
            }
            previous = iceberg;
        }
        System.out.println("Enter the number of items to be placed in the field:");
        int numberOfItems = in.nextInt();
        for (int i = 0; i < numberOfItems; ++i) {
            Rope rope = new Rope();
            Shovel shovel = new Shovel();
            DivingSuit divingSuit = new DivingSuit();
            Food food = new Food();
            items.add(rope);
            items.add(shovel);
            items.add(divingSuit);
            items.add(food);
        }

        placeFiguresOnIcebergs();
        placeItemsOnIcebergs();

        int moveChoice;
        for (int i = 0; i < players.size(); ++i) {

            for (int j = 1; j <= 4; ++j) {

                System.out.println("Welcome to the icefield!");
                System.out.println("Choose a figure type!\n");
                System.out.println("1. Explorer\n2. Eskimos");
                int figure_type = in.nextInt();
                while (figure_type != 1 && figure_type != 2) {
                    System.out.println("Invalid figure type! PLease select again!");
                    figure_type = in.nextInt();
                }
                System.out.println("PLayer " + players.get(i).getUsername() + "'s " + j + "st move");

                if (figure_type == 1) {
                    for (Explorer explorer : explorers) {
                        if (!explorer.getTaken()) {
                            players.get(i).setPlayingFigure(explorer);
                            explorer.setTaken();
                            break;
                        }
                    }
                }

                if (figure_type == 2) {
                    for (Eskimos eskimo : eskimos) {
                        if (!eskimo.getTaken()) {
                            players.get(i).setPlayingFigure(eskimo);
                            eskimo.setTaken();
                        }
                    }
                }


                System.out.println("Choose a move to go with:");

                System.out.println("1. Step to neighbouring iceberg\n");
                System.out.println("2. Remove snow\n");
                System.out.println("3. Pick item\n");
                System.out.println("4. Save figure using rope\n");
                System.out.println("5. Eat food\n");
                System.out.println("6. Wear your diving suit\n");
                System.out.println("7. Use your shovel to remove two units of snow\n");
                System.out.println("8. Check capacity of neighbouring iceberg\n");
                System.out.println("9. Build an igloo to escape the blizzard\n");
                System.out.println("10. Enter an igloo to escape the blizzard\n");


                moveChoice = in.nextInt();
                while (moveChoice == 8 && figure_type == 2) {
                    System.out.println("You are currently playing with Eskimos. Eskimos can't check capacity. Select another move\n");
                    moveChoice = in.nextInt();
                }
                while (moveChoice == 9 && figure_type == 1) {
                    System.out.println("You are currently playing with Explorer. Explorer can't build an igloo. Select another move\n");
                    moveChoice = in.nextInt();

                    while (moveChoice == 8 && figure_type == 2) {
                        System.out.println("You are currently playing with Eskimos. Eskimos can't check capacity. Select another move\n");
                        moveChoice = in.nextInt();
                    }
                    while (moveChoice == 9 && figure_type == 1) {
                        System.out.println("You are currently playing with Explorer. Explorer can't build an igloo. Select another move\n");
                        moveChoice = in.nextInt();
                    }
                    switch (moveChoice) {
                        case 10:
                        if(players.get(i).getPlayingFigure().getOnIceberg().isIglooBuilt()){
                            players.get(i).getPlayingFigure().setInsideIgloo(true);
                        }

                    }

                }


            }
        }
    }

    public void winGame() {

    }

    public void loseGame() {

    }

    public void activateBlizzard() {

    }

    public void checkSituation() {

    }

    public int generateRandomNumber(int number) {
        int random_number = 0;
        Random rand = new Random();
        while (random_number <= 0) {
            random_number = rand.nextInt(number);
        }

        return random_number;
    }

    public void placeFiguresOnIcebergs() {
        for (int i = 0; i < eskimos.size(); ++i) {
            this.icebergs.get(i).addEskimos(this.eskimos.get(i));
            this.eskimos.get(i).setOnIceberg(this.icebergs.get(i));
        }

        for (int i = 0; i < explorers.size(); ++i) {
            this.icebergs.get(i).addExplorers(this.explorers.get(i));
            this.explorers.get(i).setOnIceberg(this.icebergs.get(i));

        }
    }

    public void placeItemsOnIcebergs() {
        for (int i = 0; i < items.size(); ++i) {
            this.icebergs.get(i % 6).addItem(this.items.get(i));
        }
    }

    private int chooseMenuOption() {
        int actionNumber;
        System.out.println("Choose action for testing");
        System.out.println("1. Move to another Iceberg (func: 'stepTo')");
        System.out.println("2. Fall to hole (func: 'fallFigures')");
        System.out.println("3. Eat food (func: 'increaseBodyHeat')");
        System.out.println("4. Remove snow from Iceberg (func: 'removeSnow')");
        System.out.println("5. Collect Item (func: 'retrieveItem' ; 'collectItem')");
        System.out.println("6. Use Item [Diving Suit] (func: 'swim')");
        System.out.println("7. Use Item [Shovel] (func: 'saveFallenFigure')");
        System.out.println("8. Build Igloo [Eskimos] (func: 'buildIgloo')");
        System.out.println("9. Check Capacity [Explorer] (func: 'checkCapacity')");
        System.out.println("10. Activate Blizzard (func: 'activateBlizzard' ; 'tumbleFigures')");
        System.out.print("Enter action number: ");
        actionNumber = in.nextInt();
        if (actionNumber < 0) return -1;
        return actionNumber;
    }
}
